BINARY_PATH = ./bin

.PHONY: build
build:
	mkdir -p ./bin
	go build -v -o $(BINARY_PATH)/apiserver ./cmd/apiserver

.DEFAULT_GOAL := build