package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

type Env struct {
	db *sql.DB
}

type Config struct {
	Port int
	Db   DBconfig
}

type DBconfig struct {
	Host   string
	Port   int
	User   string
	Pass   string
	Dbname string
}

type Test struct {
	STATUS bool `json:"status"`
}

type Contact struct {
	Id        int    `json:"id"`
	Name      string `json:"name"`
	Telephone int    `json:"telephone"`
	Email     string `json:"email"`
}

type SearchQuery struct {
	Name string `json:"name"`
}

func main() {
	log.Println("App starting...")

	file, err := ioutil.ReadFile("config.json")
	if err != nil {
		log.Fatal("can't open config")
		panic(err)
	}

	var config Config

	json.Unmarshal(file, &config)

	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%v)/%s", config.Db.User, config.Db.Pass, config.Db.Host, config.Db.Port, config.Db.Dbname))
	if err != nil {
		panic(err)
	} else {
		err = db.Ping()
		if err != nil {
			log.Fatal("can't connect db")
		} else {
			log.Println("DB connected")
		}
	}

	defer db.Close()

	env := &Env{db}

	r := mux.NewRouter()
	r.HandleFunc("/", HomeHandler).Methods("GET")
	r.HandleFunc("/contacts/search", env.SearchHandler).Methods("POST")
	r.HandleFunc("/contacts", env.ListHandler).Methods("GET")
	r.HandleFunc("/contacts", env.CreateHandler).Methods("POST")
	r.HandleFunc("/contacts/{id}", env.ReadHandler).Methods("GET")
	r.HandleFunc("/contacts/{id}", env.UpdateHandler).Methods("PUT")
	r.HandleFunc("/contacts/{id}", env.DeleteHandler).Methods("DELETE")

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "DELETE", "PUT"},
		AllowCredentials: true,
	})

	handler := c.Handler(r)

	log.Println(fmt.Sprintf("Server starting on :%v", config.Port))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", config.Port), handler))

}

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("GET homepage")

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&Test{STATUS: true})
}

func (env *Env) ListHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("GET contacts list")

	rows, err := env.db.Query("SELECT * FROM list")
	if err != nil {
		panic(err)
	}

	var data []Contact

	for rows.Next() {
		var id int
		var name string
		var telephone int
		var email string
		_ = rows.Scan(&id, &name, &telephone, &email)

		data = append(data, Contact{Id: id, Name: name, Telephone: telephone, Email: email})
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}

func (env *Env) CreateHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("POST create contact")

	var insertData Contact
	json.NewDecoder(r.Body).Decode(&insertData)

	stmt, err := env.db.Prepare("INSERT list SET name=?,telephone=?,email=?")
	if err != nil {
		panic(err)
	}

	res, err := stmt.Exec(insertData.Name, insertData.Telephone, insertData.Email)
	if err != nil {
		panic(err)
	}

	id, err := res.LastInsertId()
	if err != nil {
		panic(err)
	}

	row := env.db.QueryRow("SELECT * FROM list WHERE id=? LIMIT 1", id)
	var resultData Contact
	row.Scan(&resultData.Id, &resultData.Name, &resultData.Telephone, &resultData.Email)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resultData)
}

func (env *Env) ReadHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("GET read contact")

	_vars := mux.Vars(r)
	id, _ := strconv.Atoi(_vars["id"])

	row := env.db.QueryRow("SELECT * FROM list WHERE id=? LIMIT 1", id)
	var resultData Contact
	row.Scan(&resultData.Id, &resultData.Name, &resultData.Telephone, &resultData.Email)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resultData)

	//TODO: check no row
}

func (env *Env) UpdateHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("PUT update contact")

	_vars := mux.Vars(r)
	id, _ := strconv.Atoi(_vars["id"])

	var updateData Contact
	json.NewDecoder(r.Body).Decode(&updateData)

	stmt, err := env.db.Prepare("UPDATE list SET name=? , telephone=? , email=? WHERE id=?")
	if err != nil {
		panic(err)
	}

	res, err := stmt.Exec(updateData.Name, updateData.Telephone, updateData.Email, id)
	if err != nil {
		panic(err)
	}

	log.Println(res)

	row := env.db.QueryRow("SELECT * FROM list WHERE id=? LIMIT 1", id)
	var resultData Contact
	row.Scan(&resultData.Id, &resultData.Name, &resultData.Telephone, &resultData.Email)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resultData)

	//TODO: check isset row
}

func (env *Env) DeleteHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("DELETE delete contact")

	_vars := mux.Vars(r)
	id, _ := strconv.Atoi(_vars["id"])

	stmt, err := env.db.Prepare("DELETE FROM list WHERE id=?")

	res, err := stmt.Exec(id)
	if err != nil {
		panic(err)
	}

	affect, err := res.RowsAffected()
	if err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "application/json")

	if affect == 1 {
		json.NewEncoder(w).Encode(&Test{STATUS: true})
	} else {
		json.NewEncoder(w).Encode(&Test{STATUS: false})
	}
}

func (env *Env) SearchHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("POST search contact")

	var query SearchQuery
	json.NewDecoder(r.Body).Decode(&query)

	rows, err := env.db.Query("SELECT * FROM list WHERE name LIKE ?", "%"+query.Name+"%")
	if err != nil {
		panic(err)
	}

	var data []Contact

	for rows.Next() {
		var id int
		var name string
		var telephone int
		var email string
		_ = rows.Scan(&id, &name, &telephone, &email)

		data = append(data, Contact{Id: id, Name: name, Telephone: telephone, Email: email})
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}
