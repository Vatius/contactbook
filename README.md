# contactbook

>vue.js + golang + mysql

For the front you need vue-cli. npm is recommended

## how to use

### Build front app:

1. `$ cd front`
2. `$ npm i`
3. `$ npm run build`

### Create database and table

run sql query

### Go get libs

`go get -u "github.com/rs/cors"`
`go get -u "github.com/gorilla/mux"`
`go get -u "github.com/go-sql-driver/mysql"`

### Configure and build Go app (server) 

1. create config.json (use config-dist.json for example)
2. Set http port and db access. Go application use TCP connection

## database table structure:

CREATE TABLE `list` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `telephone` BIGINT(100) NULL,
  `email` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));